var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);

var player1 = null;
var player2 = null;
var guests = [];
var order = 0;
var game = [
    null, null, null,   // 0, 1, 2
    null, null, null,   // 3, 4, 5
    null, null, null    // 6, 7, 8
];

server.listen(80);

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', function (socket) {
    var type = null;
    
    socket.emit('id', socket.id);
    
    if (!player1) {
        player1 = socket.id;
        type = 0;
        io.emit('connected', {type: 0, id: socket.id});
        console.log('player1 created');
    } else if (!player2) {
        player2 = socket.id;
        type = 1;
        io.emit('connected', {type: 1, id: socket.id});
        console.log('player2 created');
    } else {
        guests.push(socket.id);
        type = 3;
        io.emit('connected', {type: 3, id: socket.id});
        console.log('guest added');
    }
    
    socket.on('disconnect', function () {
        if (type === 0) {
            player1 = null;
            io.emit('disconnect', {type: 0, id: socket.id});
            console.log('player1 removed');
        } else if (type === 1) {
            player2 = null;
            io.emit('disconnect', {type: 1, id: socket.id});
            console.log('player2 removed');
        } else {
            var guestid = guests.findIndex(g => g === socket.id);
            guests.splice(guestid, 1);
            io.emit('disconnect', {type: 3, id: socket.id});
            console.log(socket.id + ' removed');
        }
    });
    
    socket.on('order', function (no) {
        order = no;
        io.emit('orderUpdated', no);
    });
    
    socket.on('picked', function (data) {
        io.emit('pickedUpdated', data);
    });
});
